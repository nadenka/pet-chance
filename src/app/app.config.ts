import {ApplicationConfig, importProvidersFrom} from '@angular/core';
import {provideRouter} from '@angular/router';

import {routes} from './app.routes';
import {provideNzIcons} from './icons-provider';
import {ru_RU, provideNzI18n} from 'ng-zorro-antd/i18n';
import {registerLocaleData} from '@angular/common';
import ru from '@angular/common/locales/ru';
import {FormsModule} from '@angular/forms';
import {HttpClientModule, provideHttpClient, withInterceptors} from '@angular/common/http';
import {provideAnimations} from '@angular/platform-browser/animations';
import {authInterceptor} from "./interceptors/interceptor";

registerLocaleData(ru);

export const appConfig: ApplicationConfig = {
  providers: [provideRouter(routes), provideNzIcons(), provideNzI18n(ru_RU), importProvidersFrom(FormsModule), importProvidersFrom(HttpClientModule), provideAnimations(),
    provideHttpClient(withInterceptors([authInterceptor]))]
}

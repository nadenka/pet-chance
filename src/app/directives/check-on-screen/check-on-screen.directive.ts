import {Directive, ElementRef, EventEmitter, HostListener, Input, Output} from '@angular/core';

@Directive({
  selector: '[appCheckOnScreen]',
  standalone: true
})
export class CheckOnScreenDirective {
  @Input() isHeaderOnScreen!: boolean;
  @Output() isHeaderOnScreenChange = new EventEmitter();
  @Output() isScrolled = new EventEmitter();

  constructor(private el: ElementRef<HTMLElement>) {
    this.isOnScreen();
  }

  @HostListener("window:scroll", []) onWindowScroll() {
    this.isScrolled.emit();
    this.isOnScreen();
  }

  isOnScreen() {
    setTimeout(() => {
      const result = this.el.nativeElement.getBoundingClientRect().bottom > 0;
      if (result !== this.isHeaderOnScreen) {
        this.isHeaderOnScreenChange.emit(result);
      }
    });
  }
}

import {Directive, ElementRef, Input, OnInit} from '@angular/core';

@Directive({
  selector: '[appSrc]',
  standalone: true
})
export class SrcDirective implements OnInit{
  @Input() url!: string;

  constructor(private input: ElementRef) {}

  ngOnInit(): void {
    const urlArr = this.url.split('/');
    const url = `https://www.youtube.com/embed/${urlArr[urlArr.length - 1]}`;
    this.input.nativeElement.setAttribute('src', url);
  }
}

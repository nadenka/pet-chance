import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter',
  standalone: true
})
export class FilterPipe implements PipeTransform {

  transform(data: any[], value: string): any[] {
    return data?.filter((el) => el.section === value);
  }
}

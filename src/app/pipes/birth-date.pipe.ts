import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'age',
  standalone: true
})
export class AgePipe implements PipeTransform {

  transform(years: number | undefined, months: number | undefined): string {
    let result = '';
    if (years === 1) {
      result += '1 год'
    }
    if (years && (years > 1 &&  years < 5)) {
      result += `${years} года`;
    }
    if (years && (years > 4 )) {
      result += `${years} лет`;
    }
    if (result && months) {
      result += ' ';
    }
    if (months === 1) {
      result += '1 месяц';
    }
    if (months && (months > 1 &&  months < 5)) {
      result += `${months} месяца`;
    }
    if (months && (months > 4 )) {
      result += `${months} месяцев`;
    }

    return result;
  }

}

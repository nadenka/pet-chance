import {ChangeDetectionStrategy, Component} from '@angular/core';
import {ABOUT_TEXT} from "../../../constants/constants";

@Component({
  selector: 'app-about',
  standalone: true,
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AboutComponent {
  protected readonly aboutText = ABOUT_TEXT;
}

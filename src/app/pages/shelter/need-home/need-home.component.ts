import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {NgForOf} from "@angular/common";
import {Dog} from "../../../interfaces/dog";
import {CardComponent} from "../../../helpers/card/card.component";

@Component({
  selector: 'app-need-home',
  standalone: true,
  templateUrl: './need-home.component.html',
  imports: [
    CardComponent,
    NgForOf
  ],
  styleUrls: ['./need-home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NeedHomeComponent {
  @Input() isSmallScreen = false;
  @Input() dogs!: Dog[];
  @Input() isNeedHelp = false;
  @Output() cardSelectionChange = new EventEmitter<Dog>();
}

import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'app-society',
  standalone: true,
  imports: [],
  templateUrl: './society.component.html',
  styleUrl: './society.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SocietyComponent {

}

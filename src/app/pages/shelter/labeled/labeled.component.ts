import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {Dog} from "../../../interfaces/dog";
import {NgForOf, NgIf} from "@angular/common";
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-labeled',
  standalone: true,
  templateUrl: './labeled.component.html',
  styleUrls: ['./labeled.component.scss'],
  imports: [
    NgForOf,
    NgIf
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LabeledComponent {
  api = environment.apiUrl + '/';
  @Input() dogs!: Dog[];
}

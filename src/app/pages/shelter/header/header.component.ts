import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {KeyValuePipe, NgClass, NgForOf, NgIf, NgTemplateOutlet} from "@angular/common";
import {Sections, WARRING_SECTION} from "../../../constants/constants";
import {NzButtonComponent} from "ng-zorro-antd/button";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {CheckOnScreenDirective} from "../../../directives/check-on-screen/check-on-screen.directive";

@Component({
  selector: 'app-header',
  standalone: true,
  templateUrl: './header.component.html',
  imports: [
    NgForOf,
    NgClass,
    KeyValuePipe,
    NgIf,
    NzButtonComponent,
    NgTemplateOutlet,
    CheckOnScreenDirective
  ],
  animations: [
    trigger('openClose', [
      state('open', style({
        transform: 'rotate(-90deg)'
      })),
      state('closed', style({
        transform: 'rotate(0)'
      })),
      transition('open <=> closed', [
        animate('0.1s')
      ]),
    ]),
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  protected readonly sections = Sections;
  protected readonly warringSection = WARRING_SECTION;
  isOpen = false;
  @Input() isSmallScreen = false;
  @Input() selectedSection!: string;
  @Output() selectedSectionChange = new EventEmitter<string>();

  public onChapterClick(name: string): void {
    this.selectedSection = name;
    this.selectedSectionChange.emit(name);
    this.isOpen = false;
  }
}

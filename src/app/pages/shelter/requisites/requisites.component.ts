import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'app-requisites',
  standalone: true,
  imports: [],
  templateUrl: './requisites.component.html',
  styleUrl: './requisites.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequisitesComponent {

}

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {CommonModule, DOCUMENT} from "@angular/common";
import {ActivatedRoute, Router, RouterOutlet} from "@angular/router";
import {NzModalService} from "ng-zorro-antd/modal";
import {DEFAULT_SECTION, Sections, SMALL_BREAKPOINT, WARRING_SECTION} from "../../constants/constants";
import {Subject, takeUntil, tap} from "rxjs";
import {DogModalContentComponent} from "../../modal/dog-modal-content/dog-modal-content.component";
import {IDogModalData} from "../../interfaces/dog-modal";
import {HeaderComponent} from "./header/header.component";
import {AboutComponent} from "./about/about.component";
import {NeedHomeComponent} from "./need-home/need-home.component";
import {LabeledComponent} from "./labeled/labeled.component";
import {Dog} from "../../interfaces/dog";
import {NzButtonComponent} from "ng-zorro-antd/button";
import {SocietyComponent} from "./society/society.component";
import {RequisitesComponent} from "./requisites/requisites.component";
import {CheckOnScreenDirective} from "../../directives/check-on-screen/check-on-screen.directive";
import {DogService} from "../../services/dog.service";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {FilterPipe} from "../../pipes/filter.pipe";
import {NzSpinComponent} from "ng-zorro-antd/spin";

const SECTION = 'section';

@Component({
  selector: 'app-shelter',
  standalone: true,
  imports: [
    CommonModule,
    RouterOutlet,
    HeaderComponent,
    AboutComponent,
    NeedHomeComponent,
    LabeledComponent,
    NzButtonComponent,
    SocietyComponent,
    RequisitesComponent,
    CheckOnScreenDirective,
    FilterPipe,
    NzSpinComponent
  ],
  providers: [
    NzModalService
  ],
  templateUrl: './shelter.component.html',
  styleUrls: ['./shelter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShelterComponent implements OnInit, OnDestroy {
  selectedSection = '';
  defaultSection = DEFAULT_SECTION;
  isSmallScreen: boolean = false;
  dogs$ = this.dogService.getAllShelterDogs();
  isHeaderOnScreen = true;
  protected readonly warringSection = WARRING_SECTION;
  protected readonly sections = Sections;
  private destroy$ = new Subject<void>();

  constructor(private router: Router,
              private route: ActivatedRoute,
              private modal: NzModalService,
              private cdr: ChangeDetectorRef,
              private dogService: DogService,
              @Inject(DOCUMENT) private document: Document) {
    const window = this.document.defaultView;
    if (window) {
      this.isSmallScreen = window.innerWidth <= SMALL_BREAKPOINT;
      this.cdr.markForCheck();
    }
  }

  ngOnInit() {
    const query = this.route.snapshot.queryParams;
    const selectedSection = query[SECTION];
    selectedSection ? (this.selectedSection = selectedSection) : this.tabSelectionChange(DEFAULT_SECTION);
    this.cdr.detectChanges();
    this.route.queryParams.pipe(
      tap((value) => {
        this.scrollToSection(value[SECTION]);
        this.selectedSection = value[SECTION];
      }),
      takeUntil(this.destroy$)
    ).subscribe();
  }

  tabSelectionChange(sectionName: string) {
    if (sectionName !== this.selectedSection) {
      this.selectedSection = sectionName;
      this.router.navigate(
        [],
        {
          relativeTo: this.route,
          queryParams: {section: sectionName}
        }
      );
    } else {
      this.scrollToSection(sectionName);
    }
  }

  scrollToSection(value: string) {
    const element = document.getElementById(value + 'tab-pane');
    const elementPosition = element?.getBoundingClientRect().top;
    if (elementPosition) {
      const offsetPosition = elementPosition + window.pageYOffset - 100;
      window.scrollTo({
        top: offsetPosition,
        behavior: "smooth"
      });
    }
  }

  openModal(dog: Dog) {
    this.modal.create<DogModalContentComponent, IDogModalData>({
      nzTitle: dog.name,
      nzContent: DogModalContentComponent,
      nzData: {dog},
      nzCentered: true,
      nzFooter: null,
      nzWidth: '100%',
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

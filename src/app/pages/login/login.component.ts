import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy} from '@angular/core';
import {NzButtonComponent} from "ng-zorro-antd/button";
import {Router} from "@angular/router";
import {NzInputDirective} from "ng-zorro-antd/input";
import {FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {AsyncPipe, JsonPipe, NgIf} from "@angular/common";
import {LoginService} from "../../services/login.service";
import {UserData} from "../../interfaces/user-data";
import {catchError, map, of, Subject, takeUntil} from "rxjs";

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    NzButtonComponent,
    NzInputDirective,
    NgIf,
    ReactiveFormsModule,
    AsyncPipe,
    JsonPipe
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnDestroy {
  form = new FormGroup({
    login: new FormControl('', {
      validators: [
        Validators.required,
        Validators.minLength(3)
      ]
    }),
    password: new FormControl('', {
      validators: [Validators.required]
    })
  });
  isSubmitted = false;
  isIncorrect$ = of('');
  destroy$ = new Subject();

  constructor(private router: Router,
              private loginService: LoginService,
              private cdr: ChangeDetectorRef) {

    this.form.valueChanges.pipe(
      takeUntil(this.destroy$)
    ).subscribe(() => {
      this.isSubmitted = false;
      this.isIncorrect$ = of('');
    });
  }

  navigateToShelter() {
    this.router.navigate(['shelter']);
  }

  submit = () => {
    this.form.markAsTouched();
    this.cdr.markForCheck();
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isIncorrect$ = this.loginService.login(this.form.value as UserData).pipe(
        map(() => {
          this.router.navigate(['admin']);
          return of('')
        }),
        catchError((error) => of(error))
      );
      this.isSubmitted = false;
    } else {
      this.isIncorrect$ = of('');
    }
  }

  ngOnDestroy() {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}

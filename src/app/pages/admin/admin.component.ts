import {ChangeDetectionStrategy, ChangeDetectorRef, Component, DestroyRef, inject} from '@angular/core';
import {NzOptionComponent, NzSelectComponent} from "ng-zorro-antd/select";
import {AsyncPipe, KeyValuePipe, NgForOf, NgIf} from "@angular/common";
import {ADMIN_SECTIONS, EMPTY_DOG, Sections} from "../../constants/constants";
import {FormsModule} from "@angular/forms";
import {NzTableComponent} from "ng-zorro-antd/table";
import {NzSpinComponent} from "ng-zorro-antd/spin";
import {NzDividerComponent} from "ng-zorro-antd/divider";
import {Dog} from "../../interfaces/dog";
import {catchError, finalize, map, Observable, of, switchMap} from "rxjs";
import {NzIconDirective} from "ng-zorro-antd/icon";
import {IDogModalData} from "../../interfaces/dog-modal";
import {NzModalService} from "ng-zorro-antd/modal";
import {DogEditModalContentComponent} from "../../modal/dog-edit-modal-content/dog-edit-modal-content.component";
import {AgePipe} from "../../pipes/birth-date.pipe";
import {NzButtonComponent} from "ng-zorro-antd/button";
import {NzPopoverDirective} from "ng-zorro-antd/popover";
import {DogService} from "../../services/dog.service";
import {FilterPipe} from "../../pipes/filter.pipe";
import {UploadService} from "../../services/upload.service";
import {LoginService} from "../../services/login.service";
import {takeUntilDestroyed} from "@angular/core/rxjs-interop";
import {Router} from "@angular/router";

type DogWithPopup = Dog & { isVisible?: boolean };

@Component({
  selector: 'app-admin',
  standalone: true,
  imports: [
    NzSelectComponent,
    NzOptionComponent,
    NgForOf,
    KeyValuePipe,
    NzTableComponent,
    NzSpinComponent,
    AsyncPipe,
    NzDividerComponent,
    NgIf,
    NzIconDirective,
    FormsModule,
    AgePipe,
    NzButtonComponent,
    NzPopoverDirective,
    FilterPipe
  ],
  providers: [NzModalService],
  templateUrl: './admin.component.html',
  styleUrl: './admin.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminComponent {
  protected readonly sections = ADMIN_SECTIONS;
  protected readonly allSections = Sections as Record<string, string>;
  private destroyRef = inject(DestroyRef);
  section = ADMIN_SECTIONS[0];
  allDogs$: Observable<DogWithPopup[]> = this.service.getAllDogs().pipe(
    map((dogs) => (dogs.map((dog) => ({...dog, isVisible: false}))))
  );

  constructor(private modal: NzModalService,
              private cdr: ChangeDetectorRef,
              private uploadService: UploadService,
              private loginService: LoginService,
              private router: Router,
              private service: DogService) {
  }

  editDog(dogs: DogWithPopup[], dog: Dog) {
    this.openModal(dogs, dog);
  }

  openModal(dogs: DogWithPopup[], dog: Dog) {
    this.modal.create<DogEditModalContentComponent, IDogModalData>({
      nzTitle: dog.name,
      nzContent: DogEditModalContentComponent,
      nzData: {dog},
      nzCentered: true,
      nzWidth: '100%',
      nzOnOk: (data) => {
        const newDog = {...data.dog.value, section: this.section};
        const id = dog._id;
        if (id && data.dog.touched) {
          this.allDogs$ = this.uploadService.uploadFile(data.src!).pipe(
            switchMap((result) => {
              if (result) {
                newDog.src = result;
              }
              return this.uploadService.uploadFiles(data.pictures!)
            }),
            switchMap((result) => {
              if (result) {
                newDog.pictures = newDog.pictures?.length ?
                  [...newDog.pictures!, ...result] :
                  result as string[];
              }
              return this.service.editDog(newDog as Dog, id)
            }),
            map(() => {
              const index = dogs.findIndex((el) => el._id === id);
              dogs[index] = {...newDog as Dog, _id: id, isVisible: false};
              return dogs;
            }),
            catchError(() => of(dogs)),
            finalize(() => this.cdr.markForCheck())
          )
        } else if (!id) {
          this.allDogs$ = this.uploadService.uploadFile(data.src!).pipe(
            switchMap((result) => {
              if (result) {
                newDog.src = result as string;
              }
              return this.uploadService.uploadFiles(data.pictures!)
            }),
            switchMap((result) => {
              if (result) {
                newDog.pictures = result as string[];
              }
              return this.service.addDog(newDog as Dog)
            }),
            map((res) => ([...dogs, {...newDog as Dog, _id: res, isVisible: false}])),
            finalize(() => this.cdr.markForCheck()));
        }
        this.modal.closeAll();
        this.cdr.markForCheck();
        return false;
      }
    });
  }

  addDog(dogs: DogWithPopup[]) {
    this.openModal(dogs, {...EMPTY_DOG, section: Sections[this.section as 'home']});
  }

  removeDog(dogs: DogWithPopup[], id?: string) {
    if (id) {
      this.allDogs$ = this.service.removeDog(id).pipe(
        map(() => dogs.filter((el) => el._id !== id)),
        finalize(() => this.cdr.markForCheck())
      );
    }
  }

  sectionChange() {
    this.cdr.markForCheck();
  }

  changePopup(data: DogWithPopup) {
    data.isVisible = !data.isVisible;
    this.cdr.markForCheck();
  }

  logout() {
    this.loginService.logout().pipe(
      takeUntilDestroyed(this.destroyRef)
    ).subscribe(() => this.router.navigate(['/login']))
  }
}

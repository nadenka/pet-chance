import {CanActivateFn} from "@angular/router";
import {inject} from "@angular/core";
import {LoginService} from "../services/login.service";

export const authGuard: CanActivateFn = () => {
  const service = inject(LoginService);

  return service.isAuthorised$.value ? service.isAuthorised$ : service.checkAuthorization();
};

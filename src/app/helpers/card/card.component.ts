import {ChangeDetectionStrategy, Component, Inject, Input} from '@angular/core';
import {NzCardComponent, NzCardMetaComponent} from "ng-zorro-antd/card";
import {NgIf} from "@angular/common";
import {NzCarouselComponent} from "ng-zorro-antd/carousel";
import {Dog} from "../../interfaces/dog";
import {WARRING_SECTION} from "../../constants/constants";
import {AgePipe} from "../../pipes/birth-date.pipe";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-card',
  standalone: true,
  templateUrl: './card.component.html',
  imports: [
    NzCardComponent,
    NzCardMetaComponent,
    NzCarouselComponent,
    NgIf,
    AgePipe
  ],
  styleUrls: ['./card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardComponent {
  api = environment.apiUrl + '/';
  @Input() isSmallScreen = false;
  @Input() card!: Dog;
  @Input() isNeedHelp = false;

  protected readonly warringSection = WARRING_SECTION;
}

import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {NgForOf, NgIf} from "@angular/common";

@Component({
  selector: 'app-upload-pictures',
  standalone: true,
  imports: [
    NgForOf,
    NgIf
  ],
  templateUrl: './upload-pictures.component.html',
  styleUrl: './upload-pictures.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UploadPicturesComponent {
  @Input() pictures: string[] = [];
  @Input() api = '';
  @Output() picturesChange = new EventEmitter<string[]>();
  @Output() deleteId = new EventEmitter<number>();

  deleteUploadedPhoto(index: number) {
    this.pictures = this.pictures?.filter((_, id) => id !== index);
    this.picturesChange.emit(this.pictures);
    this.deleteId.emit(index);
  }
}

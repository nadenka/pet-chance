import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, Output} from '@angular/core';
import {NzButtonComponent} from "ng-zorro-antd/button";
import {JsonPipe, NgForOf, NgIf} from "@angular/common";
import {UploadPicturesComponent} from "../upload-pictures/upload-pictures.component";

@Component({
  selector: 'app-upload',
  standalone: true,
  templateUrl: './upload.component.html',
  imports: [
    NzButtonComponent,
    NgIf,
    NgForOf,
    JsonPipe,
    UploadPicturesComponent
  ],
  styleUrl: './upload.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UploadComponent {
  @Input() isMultiple = true;
  @Input() src: string[] | null = [];
  @Output() filesChange = new EventEmitter<File[] | null>();
  files = [];

  constructor(private cdr: ChangeDetectorRef) {
  }

  onFileChange(event: any) {
    this.files = event.target.files;
    if (!this.isMultiple) {
      const reader = new FileReader();
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.src = [reader.result] as (string & ArrayBuffer)[];
        this.cdr.markForCheck();
      };
    } else {
      for (let file of this.files) {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.src = [...this.src as string[], reader.result as string];
          this.cdr.markForCheck();
        };
      }
    }
    this.cdr.markForCheck();
    this.filesChange.emit(this.files);
  }

  deleteSrc() {
    this.src = [];
    this.filesChange.emit(null);
  }

  deletePhoto(id: number) {
    this.src = this.src!.filter((el, i) => i !== id);
    this.files = this.files.filter((_, i) => i !== id);
    this.filesChange.emit(this.files);
  }
}

import {ChangeDetectionStrategy, Component, inject, OnDestroy, OnInit} from '@angular/core';
import {NgForOf, NgIf} from "@angular/common";
import {NZ_MODAL_DATA} from "ng-zorro-antd/modal";
import {IDogModalData} from "../../interfaces/dog-modal";
import {SrcDirective} from "../../directives/src/src.directive";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-dog-modal-content',
  standalone: true,
  imports: [
    NgForOf,
    NgIf,
    SrcDirective
  ],
  templateUrl: './dog-modal-content.component.html',
  styleUrl: './dog-modal-content.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DogModalContentComponent implements OnInit, OnDestroy{
  readonly data: IDogModalData = inject(NZ_MODAL_DATA);
  videoTMP = '';
  api = environment.apiUrl + '/';

  ngOnInit() {
    document.body.style.overflow = 'hidden';
    this.videoTMP = `<iframe width="420" height="345" src="${this.data.dog.video}"></iframe>`;
  }

  ngOnDestroy() {
    document.body.style.overflow = 'auto';
  }
}

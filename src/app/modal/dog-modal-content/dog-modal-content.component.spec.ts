import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DogModalContentComponent } from './dog-modal-content.component';

describe('DogModalContentComponent', () => {
  let component: DogModalContentComponent;
  let fixture: ComponentFixture<DogModalContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DogModalContentComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DogModalContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

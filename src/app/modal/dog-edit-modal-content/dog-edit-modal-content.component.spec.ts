import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DogEditModalContentComponent } from './dog-edit-modal-content.component';

describe('DogEditModalContentComponent', () => {
  let component: DogEditModalContentComponent;
  let fixture: ComponentFixture<DogEditModalContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DogEditModalContentComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DogEditModalContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

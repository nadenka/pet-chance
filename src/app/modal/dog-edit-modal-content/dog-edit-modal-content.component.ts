import {ChangeDetectionStrategy, Component, inject, OnDestroy, OnInit} from '@angular/core';
import {IDogModalData} from "../../interfaces/dog-modal";
import {NZ_MODAL_DATA} from "ng-zorro-antd/modal";
import {NzAutosizeDirective, NzInputDirective} from "ng-zorro-antd/input";
import {NzOptionComponent, NzSelectComponent} from "ng-zorro-antd/select";
import {FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {SEX_VARIANTS} from "../../constants/constants";
import {NgForOf, NgIf} from "@angular/common";
import {NzButtonComponent} from "ng-zorro-antd/button";
import {UploadComponent} from "../../helpers/upload/upload.component";
import {Subject} from "rxjs";
import {environment} from "../../../environments/environment";
import {UploadPicturesComponent} from "../../helpers/upload-pictures/upload-pictures.component";

@Component({
  selector: 'app-dog-edit-modal-content',
  standalone: true,
  imports: [
    NzInputDirective,
    NzSelectComponent,
    ReactiveFormsModule,
    NzOptionComponent,
    NgForOf,
    NzAutosizeDirective,
    NzButtonComponent,
    UploadComponent,
    NgIf,
    UploadPicturesComponent
  ],
  templateUrl: './dog-edit-modal-content.component.html',
  styleUrl: './dog-edit-modal-content.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DogEditModalContentComponent implements OnInit, OnDestroy {
  readonly data: IDogModalData = inject(NZ_MODAL_DATA);
  readonly sexVariants = SEX_VARIANTS;
  private destroy$ = new Subject();
  api = environment.apiUrl + '/';
  src: File | null = null;
  pictures: File[] | null = null;
  dog = new FormGroup({
    mark: new FormControl(),
    sex: new FormControl('', [Validators.required]),
    name: new FormControl(''),
    years: new FormControl(),
    months: new FormControl(),
    address: new FormControl(''),
    description: new FormControl(''),
    src: new FormControl(''),
    pictures: new FormControl([] as string[]),
    video: new FormControl(),
    accountData: new FormControl()
  });
  ngOnInit() {
    this.dog.patchValue(this.data.dog);
  }

  addFile(files: File[] | null) {
    if (files) {
      this.src = files[0];
    } else {
      this.src = null;
      this.dog.patchValue({src: ''})
    }
    this.dog.markAsTouched();
  }

  addFiles(files: File[] | null) {
    if (files) {
      this.pictures = files;
    } else {
      this.pictures = null;
      this.dog.patchValue({pictures: []})
    }
    this.dog.markAsTouched();
  }

  changePhotos(pictures: string[]) {
    this.dog.patchValue({pictures});
    this.dog.markAsTouched();
  }

  ngOnDestroy() {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}

import {Dog} from "../interfaces/dog";

export enum Sections {
  about = 'О нас',
  home = 'Ищут дом',
  help = 'Нужна помощь',
  labeled = 'Биркованные собаки',
  requisites = 'Реквезиты для помощи',
  society = 'Контакты'
}

export const ADMIN_SECTIONS = ['home', 'help', 'labeled'];

export const SMALL_BREAKPOINT = 768;
export const MIDDLE_BREAKPOINT = 1060;

export const DEFAULT_SECTION = 'about';
export const WARRING_SECTION = 'help';

export const SEX_VARIANTS = ['девочка', 'мальчик', '?'];

export const EMPTY_DOG: Dog = {
  mark: '',
  sex: '?',
  name: '',
  years: undefined,
  months: undefined,
  address: '',
  description: '',
  src: '',
  pictures: [],
  accountData: '',
  video: '',
  section: Sections.home,
  _id: ''
}

export const ABOUT_TEXT = `<h4>Приют "Шанс" Построен в 2023 году, в чистом поле без света и воды, с абсолютного нуля, только за счёт помощи неравнодушных людей.<br>В нём содержатся животные, которые оказались брошенными, или которые попали в беду. Мы подбираем, лечим их, выхаживаем, пытаемся найти дом. Но большинство так и остаётся в приюте...<br>Помимо собак в приюте содержатся лошадки выкупленные с бойни, куда их сдали прошлые владельцы. В приюте они получили второй Шанс на жизнь.<br>Приют содержится только за счёт пожертвований и всегда нуждается в помощи</h4>`;

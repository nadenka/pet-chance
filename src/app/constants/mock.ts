import {Dog} from "../interfaces/dog";
import {Sections} from "./constants";

export const dogs: Dog[] = [
  {
    name: 'Девочка',
    _id: 'sdfs',
    src: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSZjw9azAPDq1PANh_GGTTYcS0uMhATMlANpXMMcB2dBw&s',
    description: 'Прекрасная собака, агнгел, моя принцесса, солныщко, умница. Прекрасная собака,Прекрасная собака,Прекрасная собака,Прекрасная собака,Прекрасная собака,Прекрасная собака,Прекрасная собака,Прекрасная собака,Прекрасная собака,Прекрасная собака,Прекрасная собака,Прекрасная собака,Прекрасная собака,Прекрасная собака,Прекрасная собака,Прекрасная собака,',
    pictures: [
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSZjw9azAPDq1PANh_GGTTYcS0uMhATMlANpXMMcB2dBw&s',
      'https://hips.hearstapps.com/hmg-prod/images/dog-puppy-on-garden-royalty-free-image-1586966191.jpg?crop=0.752xw:1.00xh;0.175xw,0&resize=1200:*',
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSAYSQius5QoYKWXQY3k6F9pSUmJG2VfUGTbM2HPAdkOw&s',
      'https://www.princeton.edu/sites/default/files/styles/1x_full_2x_half_crop/public/images/2022/02/KOA_Nassau_2697x1517.jpg?itok=Bg2K7j7J',
      'https://hips.hearstapps.com/hmg-prod/images/beautiful-australian-shepherd-walking-royalty-free-image-168814214-1565190235.jpg?crop=0.66832xw:1xh;center,top&resize=980:*'
    ],
    accountData: 'Сбербанк 0000000000000000',
    sex: '?',
    mark: '#12345',
    years: 5,
    months: 2,
    address: 'Деревня',
    video: 'https://www.youtube.com/shorts/_2Sx8emiKws',
    section: Sections.home
  },
  {
    name: 'Принцесса',
    _id: 'sdfsdfsdfsfsasdad',
    src: 'https://zoobonus.ua/storage/breeds/images/big/HyswT7ZbpykGWAJwVO2UwvAjy85pLsNMy6OepoRR.webp',
    description: 'Прекрасная собака, агнгел, моя принцесса, солныщко, умница',
    pictures: ['https://zoobonus.ua/storage/breeds/images/big/HyswT7ZbpykGWAJwVO2UwvAjy85pLsNMy6OepoRR.webp'],
    accountData: 'Сбербанк 0000000000000000',
    sex: 'девочка',
    mark: '',
    years: 0,
    months: 11,
    address: '',
    video: 'https://youtu.be/niGystfKJn4',
    section: Sections.help
  }, {
    name: 'Волосака',
    _id: 'ghjghj',
    src: 'https://petstory.ru/resize/800x800x80/upload/images/articles/breeds/catalan-sheepdog/2.jpg',
    description: 'Прекрасная собака, агнгел, моя принцесса, солныщко, умница',
    pictures: ['https://petstory.ru/resize/800x800x80/upload/images/articles/breeds/catalan-sheepdog/2.jpg'],
    accountData: 'Сбербанк 0000000000000000',
    sex: '?',
    mark: '#12345',
    years: 5,
    months: 1,
    address: 'Рабоче-Крестьянская ул., 42, Волгоград, Волгоградская обл., Россия, 400074',
    video: 'https://www.youtube.com/embed/z6EchXyieos',
    section: Sections.labeled
  }, {
    name: 'Малыш',
    _id: '45645654',
    src: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTb7HSM_NemiUNylrBc8vJtaEKbaLKfW2oKHm9aySO4Yg&s',
    description: 'Прекрасная собака, агнгел, моя принцесса, солныщко, умница',
    pictures: ['https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTb7HSM_NemiUNylrBc8vJtaEKbaLKfW2oKHm9aySO4Yg&s'],
    accountData: 'Сбербанк 0000000000000000',
    sex: '?',
    mark: '#12345',
    years: undefined,
    months: undefined,
    address: 'Деревня',
    video: 'https://youtu.be/-LFSpfxBcm4',
    section: Sections.labeled
  }
];

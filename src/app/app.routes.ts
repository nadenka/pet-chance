import {Routes} from '@angular/router';
import {ShelterComponent} from "./pages/shelter/shelter.component";
import {AdminComponent} from "./pages/admin/admin.component";
import {LoginComponent} from "./pages/login/login.component";
import {authGuard} from "./guards/auth-guard";

export const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'shelter'},
  {path: 'shelter', component: ShelterComponent},
  {path: 'admin', component: AdminComponent, canActivate: [authGuard]},
  {path: 'login', component: LoginComponent},
  {path: '**', redirectTo: 'shelter', pathMatch: 'full' }
];

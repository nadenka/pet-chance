import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Dog} from "../interfaces/dog";
import {CalculationService} from "./calculation.service";
import {catchError, map, Observable, of, shareReplay} from "rxjs";
import {DogWithDate} from "../interfaces/dog-with-date";

@Injectable({
  providedIn: 'root'
})
export class DogService {
  constructor(private http: HttpClient,
              private calculationService: CalculationService) {
  }

  getAllDogs() {
    return this.http.get<DogWithDate[]>('/dogs').pipe(
      map((data) => (data?.map((dog) => ({...dog, ...this.calculationService.getYearAndMonth(dog.birthDate)} as Dog)))),
      catchError(() => of([]))
    );
  }

  getAllShelterDogs() {
    return this.http.get<DogWithDate[]>('/dogs').pipe(
      map((data) => (data?.map((dog) => ({...dog, ...this.calculationService.getYearAndMonth(dog.birthDate)} as Dog)))),
      shareReplay(1),
      catchError(() => of([]))
    );
  }

  removeDog(id: string) {
    return this.http.delete<string>(`/dog/${id}`);
  }

  addDog(dog: Dog): Observable<string> {
    const result = {...dog, birthDate: this.calculationService.getBirthDate(dog.years, dog.months)};
    return this.http.post<string>('/dog', result);
  }

  editDog(dog: Dog, id: string) {
    const result = {...dog, birthDate: this.calculationService.getBirthDate(dog.years, dog.months)};
    return this.http.put<string>(`/dog/${id}`, result);
  }
}

import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {UserData} from "../interfaces/user-data";
import {BehaviorSubject, catchError, of, tap} from "rxjs";


interface UserToken {
  access: string;
  refresh: string;
  accessDate: string;
  refreshDate: string;
}
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  isAuthorised$ = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) {
  }

  login(userData: UserData) {
    return this.http.post<UserToken>('/login', userData).pipe(
      tap((result) => {
        localStorage.setItem('access', result.access);
        localStorage.setItem('refresh', result.refresh);
        localStorage.setItem('accessDate', result.accessDate + '');
        localStorage.setItem('refreshDate', result.refreshDate + '');
        this.changeAuthorized(true);
      }));
  }

  checkAuthorization() {
    const token = localStorage.getItem('access') || '';
    return this.http.post<boolean>('/login/check', token);
  }

  checkRefreshToken() {
    const token = localStorage.getItem('refresh') || '';
    const date = localStorage.getItem('refreshDate') || '';
    return this.http.post<UserToken>('/login/check-refresh', { token, date }).pipe(
      tap((res) => {
        localStorage.setItem('accessDate', res.accessDate + '');
        localStorage.setItem('access', res.access);
        localStorage.setItem('refresh', res.refresh);
        this.changeAuthorized(true);
      })
    );
  }

  changeAuthorized(value: boolean) {
    this.isAuthorised$.next(value);
  }

  logout() {
    const token = localStorage.getItem('refresh') || '';
    return this.http.post('/login/logout', {token}).pipe(
      catchError(() => of({})),
      tap(() => {
        localStorage.removeItem('access');
        localStorage.removeItem('refresh');
        localStorage.removeItem('accessDate');
        localStorage.removeItem('refreshDate');
        this.changeAuthorized(false);
      }));
  }
}

import {Injectable} from '@angular/core';

interface DATE {
  months: number | undefined,
  years: number | undefined
}

@Injectable({
  providedIn: 'root'
})
export class CalculationService {
  getBirthDate(years: number | undefined, months: number | undefined): string {
    if (!years && !months) {
      return '';
    }
    const date = new Date();
    if (years) {
      date.setFullYear(date.getFullYear() - years);
    }

    if (months) {
      date.setMonth(date.getMonth() - months);
    }
    return date.toISOString();
  }

  getYearAndMonth(date: string): DATE {
    const today = new Date();
    const birthDate = new Date(date);
    let years = today.getFullYear() - birthDate.getFullYear();
    const months = today.getMonth() - birthDate.getMonth();
    if (months < 0 || (months === 0 && today.getDate() < birthDate.getDate())) {
      years--;
    }
    return {years, months: months < 0 ? months + 12 : months}
  }
}

import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UploadService {
  constructor(private http: HttpClient) {
  }

  uploadFile(file: File) {
    let formParams = new FormData();
    formParams.append('file', file)
    if (file) {
      return this.http.post<string>('/photo', formParams);
    }
    return of('');
  }

  uploadFiles(files: File[]) {
    if (files) {
      let formParams = new FormData();
      for(let file of files) {
        formParams.append(`file`, file);
      }
      return this.http.post<string[]>('/photos', formParams);
    }
    return of('');
  }

  deleteFile(name: string) {
    return this.http.delete('/photo', {body: {name}});
  }
}

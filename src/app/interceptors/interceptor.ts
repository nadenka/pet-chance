import {HttpErrorResponse, HttpInterceptorFn} from '@angular/common/http';
import {environment} from "../../environments/environment";
import {catchError, switchMap} from "rxjs";
import {inject} from "@angular/core";
import {LoginService} from "../services/login.service";
import {Router} from "@angular/router";

export const authInterceptor: HttpInterceptorFn = (req, next) => {
  const api = environment.apiUrl;
  let token = localStorage.getItem('access') || '';
  const loginService = inject(LoginService);
  const router = inject(Router);
  let date = localStorage.getItem('accessDate') || '';

  const cloned = req.clone({
    url: api + req.url,
    setHeaders: {authorization: token, "last-modified": date },
  });

  const isRefresh = cloned.url.includes('check-refresh');

  return next(cloned).pipe(
    catchError((err) => {
      if (err instanceof HttpErrorResponse && err.status === 401 && !isRefresh) {
        return loginService.checkRefreshToken().pipe(
          switchMap((res) => {
            token = res.access;
            date = localStorage.getItem('accessDate') || '';
            return next(req.clone({
              url: api + req.url,
              setHeaders: {authorization: token, "last-modified": date },
            }));
          }),
          catchError((err) => {
            loginService.changeAuthorized(false);
            router.navigate(['/login']);
            throw err;
          })
        )
      }
      throw err;
    })
  );
};

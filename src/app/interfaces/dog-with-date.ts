import {Sections} from "../constants/constants";

export interface DogWithDate {
  mark: string;
  sex: 'девочка' | 'мальчик' | '?';
  name: string;
  address: string;
  description: string;
  src: string;
  pictures: string[];
  accountData: string;
  video: string;
  section: Sections;
  birthDate: string;
  _id?: string;
}

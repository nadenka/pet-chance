import {Dog} from "./dog";

export interface IDogModalData {
  dog: Dog
}
